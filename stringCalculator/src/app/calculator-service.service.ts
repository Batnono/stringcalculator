import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculatorServiceService {

  constructor() { }

public add(numbers: string): number {
  let delimiter: string = '';
  if(numbers.includes('//')) {
    delimiter = numbers.split('\n')[0].replace('//','');
    numbers = numbers.split('\n').pop();
  }
  const numbersArray = numbers.trim().replace(delimiter, ',').replace('\n',',').split(',').map(val => +val);
  if(numbersArray.some(nmbr => nmbr < 0)) {
    throw new Error('negatives not allowed : ' + numbersArray.filter(nmbr => nmbr < 0).join(', '));
  }
  return numbersArray.reduce((sum, current) => sum + current);
}

}
