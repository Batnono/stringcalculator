import { TestBed } from '@angular/core/testing';

import { CalculatorServiceService } from './calculator-service.service';

describe('CalculatorServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalculatorServiceService = TestBed.get(CalculatorServiceService);
    expect(service).toBeTruthy();
  });


  it('with two numbers, should return 5', () => {
    const service: CalculatorServiceService = TestBed.get(CalculatorServiceService);
    expect(service.add('3, 2')).toBe(5);
  });

  it('with empty string, should return 0', () => {
    const service: CalculatorServiceService = TestBed.get(CalculatorServiceService);
    expect(service.add('')).toBe(0);
  });

  it('with morethan 2 numbers, should return 24', () => {
    const service: CalculatorServiceService = TestBed.get(CalculatorServiceService);
    expect(service.add('4, 5,8, 3, 2, 2')).toBe(24);
  });

  it('with newLine, should return 9', () => {
    const service: CalculatorServiceService = TestBed.get(CalculatorServiceService);
    expect(service.add('4, 2\n3')).toBe(9);
  });

  it('with delimiter, should return 7', () => {
    const service: CalculatorServiceService = TestBed.get(CalculatorServiceService);
    expect(service.add('//;\n3;4')).toBe(7);
  });

  it('with one negative, should throw Error', () => {
    const service: CalculatorServiceService = TestBed.get(CalculatorServiceService);
    expect(() => service.add('5, 5, -4')).toThrowError('negatives not allowed : -4');
  });

  it('with two negatives, should throw Error', () => {
    const service: CalculatorServiceService = TestBed.get(CalculatorServiceService);
    expect(() => service.add('5, 5, -4, -8')).toThrowError('negatives not allowed : -4, -8');
  });

});
